'use strict';

var pagination = require('hexo-pagination');

module.exports = function(locals){
	var config = this.config;
	var perPage = config.index_generator.per_page;
	var paginationDir = config.pagination_dir || 'page';
	var sortedposts=locals.posts.sort(config.index_generator.order_by)

	var locales = Array.isArray(config.language) ? config.language  :  [].push(config.language);
	return locales.reduce(function(result,locale) {

		var posts= sortedposts.filter(function(post) {
			return (post.lang=== locale)
		});
		var data = pagination(locale, posts, {
			perPage: perPage,
			layout: [ 'archive', 'index'],
			format: paginationDir + '/%d/',
			data: {}
		});

		return result.concat(data);


	},[]);


};