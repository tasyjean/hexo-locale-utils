'use strict';

var _ = require('lodash');


module.exports = function(locals){
	var config = this.config;
	var posts=locals.posts.sort(config.index_generator.order_by).toArray();
	var length = posts.length;

	return posts.map(function(post, i){
		var layout = post.layout;
		var path = post.path;
		var recents=[];

		if (!layout || layout === 'false'){
			return {
				path: path,
				data: post.content
			};
		} else {
			

			for (var x= i-1 ; x>=0; x--) {

				if (post.lang===posts[x].lang) {
					post.prev = posts[x];
					break;
				}
				
			};	
			for ( var x= i+1; x<length; x++) {
				
				if (post.lang===posts[x].lang) {
					post.next = posts[x];
					break;
				}
				
			};


			for ( var x=length;x>=0; x--) {
				if(posts.length < 4){
					if ((i !== x)&& (post.lang===posts[x].lang )  ){
						recents.push(posts[x]);
					}
				}else{
					break;
				}
				
			};
			post.recents =recents;
			
			

			var layouts = ['post', 'page', 'index'];
			if (layout !== 'post') layouts.unshift(layout);

			return {
				path: path,
				layout: layouts,
				data: _.extend({
					__post: true
				}, post)
			};
		}
	});
}







