'use strict';

var pagination = require('hexo-pagination');

module.exports = function(locals) {
  var config = this.config;
  var defaultLang = Array.isArray(config.language) ? config.language[0]  :  config.language;
  var paginationDir = config.pagination_dir || 'page';

  var posts = locals.posts.sort(config.index_generator.order_by).filter(function(post) {
      return (post.lang=== defaultLang)
    });


  return pagination('', posts, {
    perPage: config.index_generator.per_page,
    layout: ['index', 'archive'],
    format: paginationDir + '/%d/',
    data: {
      __index: true
    }
  });
};
