'use strict';

var assign = require('object-assign');


hexo.config.index_generator = assign({
  per_page: typeof hexo.config.per_page === 'undefined' ? 10 : hexo.config.per_page,
  order_by: '-date'
}, hexo.config.index_generator);



hexo.extend.generator.register('paginator', require('./lib/pagin'));
hexo.extend.generator.register('index', require('./lib/home'));
hexo.extend.generator.register('post', require('./lib/post'));
hexo.extend.helper.register('url_for_locale', require('./lib/url'));
